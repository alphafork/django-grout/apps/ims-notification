from django.contrib.auth.models import Group, User
from django.db import models
from ims_base.models import AbstractLog


class UserNotification(AbstractLog):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    notification = models.TextField()
    notification_time = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)

    def __str__(self) -> str:
        return f"{self.user.get_full_name()} | {self.notification_time}"


class GroupNotification(AbstractLog):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    notification = models.TextField()
    notification_time = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return f"{self.group.name} | {self.notification_time}"
