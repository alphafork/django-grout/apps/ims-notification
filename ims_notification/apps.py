from django.apps import AppConfig


class IMSNotificationConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_notification"

    model_strings = {
        "USER_NOTIFICATION": "UserNotification",
        "GROUP_NOTIFICATION": "GroupNotification",
    }
