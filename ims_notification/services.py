from django.contrib.auth.models import Group, User

from ims_notification.models import GroupNotification, UserNotification


def add_group_notification(notification, group_names):
    messaged_groups = []
    for group_name in group_names:
        group = Group.objects.get(name=group_name)
        group_notification = GroupNotification.objects.create(
            notification=notification,
            group=group,
        )
        users = User.objects.filter(groups__name=group_name).exclude(
            groups__name__in=messaged_groups
        )
        for user in users:
            UserNotification.objects.create(
                notification=notification,
                notification_time=group_notification.notification_time,
                user=user,
            )
        messaged_groups.append(group_name)
