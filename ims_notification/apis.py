from ims_base.apis import BaseAPIViewSet


class UserNotificationAPI(BaseAPIViewSet):
    object_user_allowed_methods = ["HEAD", "OPTIONS", "GET", "PATCH", "PUT"]

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)
